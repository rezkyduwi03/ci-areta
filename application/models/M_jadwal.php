<?php
class M_jadwal extends CI_Model {

	function tampil()
	{
		$jadwal=$this->db->get('v_jadwal'); //ini sama dengan select * from siswa kalau di CMD 
		return $jadwal; //2 baris menampilkan database
	}

	function simpan($data)
	{
		$this->db->insert('jadwal',$data);
	}

	function getId($data)
	{
		$param=array('id_jadwal'=>$data);
		return $this->db->get_where('jadwal',$param);
	}

	function update($data,$id)
	{
		$this->db->where('id_jadwal',$id);
		$this->db->update('jadwal',$data);
	}
}