<?php
class login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	function index() //ini untuk menampilakan halaman login
	{
		$judul="Halaman Login";
		$data['judul']=$judul;
		$this->load->view('login',$data);
	}

	function auth() //ini untuk mengecek login
	{
		$username = $this->input->post('username');//inputan
		$password = $this->input->post('password');//inputan
		$cek = $this->M_login->login($username,$password);
		if($cek == 1)
		{

			$newdata = array(//pendaftaran session
				'username' =>$username,
				'logged_in' =>True
			);

		$this->session->set_userdata($newdata);//tyty
		
          redirect ('dashbord');
		}
		else
		{
		  redirect('login');
		}
	}
	
   function logout()//ini untuk logout
   {
   		session_destroy();//mengapus semuah session yang ada
   		redirect('login');// diarahkan ke halaman login

   }

   function sign()
   {
   	$data['title']='login'
   	$this->load->view('Sign',$data);
   }
}