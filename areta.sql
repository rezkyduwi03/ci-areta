-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16 Des 2018 pada 07.27
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `areta`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `nik` int(10) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `alamat` text,
  `hp` varchar(13) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `pasword` varchar(20) DEFAULT NULL,
  `tanggal_lahir` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`nik`, `nama`, `email`, `alamat`, `hp`, `status`, `jenis_kelamin`, `pasword`, `tanggal_lahir`) VALUES
(2018001, 'Pak Budi', 'budi@gmail.com', 'Dasana Indah', '085213519560', 'Belum Menikah', 'Lk', 'sukses889', '12 Desember 2018'),
(2018002, 'Ibu Sulis', 'sulis@gmail.com', 'Dasana Indah', '085213160835', 'Belum Menikah', 'Pr', 'sukses889', '20 Juli 1990'),
(2018003, 'Febryo Ponco S.', 'febryoponco@gmail.com', 'Tangerang', '085213160835', 'Menikah', 'Laki-laki', 'sukses89', '12 Agustus 1989'),
(2018004, 'Sigit Pamungkas', 'sigitpamungkas@gmail.com', 'Tangerang', '085211530846', 'Menikah', 'Laki-laki', 'sukses88', '20 April 1985'),
(2018005, 'Abu Bakar', 'abubakar@gmail.com', 'Riau', '085211530846', 'Menikah', 'Laki-laki', 'rindu12', '13 Mei 1977'),
(2018006, 'Fitriani', 'fitriani@gmail.com', 'Cikokol', '085211235670', 'Belum Menikah', 'Perempuan', 'rinduayah', '16 Februari 1993'),
(2018007, 'Nursiah', 'nursiah@gmail.com', 'Enok Riau', '085213519566', 'Menikah', 'Perempuan', 'rindu10', '13 Mei 1977');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(3) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `jam` varchar(15) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `kd_kelas` varchar(10) NOT NULL,
  `kd_mapel` varchar(5) NOT NULL,
  `kd_ruang` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `nik`, `jam`, `hari`, `kd_kelas`, `kd_mapel`, `kd_ruang`) VALUES
(1, '2018001', '13:00 - 16:00', 'Senin', '1', 'L01', 'L1'),
(2, '2018002', '16:00 - 20:00', 'Senin', '2', 'W01', 'L2'),
(3, '2018003', '12.00 -14.00', 'Kamis', '1', 'L01', 'L1'),
(4, '2018005', '12.00 - 14.00', 'Jum\'at', '2', 'W01', 'L2'),
(5, '2018004', '12.00 - 14.00', 'Jum\'at', '1', 'L01', 'L1'),
(6, '2018006', '08.00 - 10.00', 'Rabu', '2', 'DS01', 'L2'),
(7, '2018005', '09.00 - 11.00', 'Selasa', '4', 'DB01', 'L3'),
(8, '2018002', '08.00 - 10.00', 'Senin', '3', 'DB01', 'L3'),
(9, '2018007', '09.00 - 11.00', 'Jum\'at', '4', 'CB01', 'L5'),
(10, '2018001', '21.00-22.00', 'Selasa', '3', 'L01', 'L1'),
(11, '2018005', '12.00 - 14.00', 'Kamis', '1', 'L01', 'L1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `kd_kelas` varchar(10) NOT NULL,
  `nama` varchar(10) DEFAULT NULL,
  `jumlah` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`kd_kelas`, `nama`, `jumlah`) VALUES
('1', '2A', 10),
('2', '3A', 11),
('3', '4A', 12),
('4', '5A', 13),
('6', '6A', 14);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `kd_mapel` varchar(5) DEFAULT NULL,
  `nama_mapel` varchar(200) DEFAULT NULL,
  `sks` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`kd_mapel`, `nama_mapel`, `sks`) VALUES
('L01', 'Linux Administrator', 4),
('W01', 'Code Igniter', 4),
('DS01', 'Design Grafis', 4),
('DB01', 'Database with MySQL', 4),
('CB01', 'Character Building', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `kd_ruang` varchar(4) NOT NULL,
  `nama_ruang` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`kd_ruang`, `nama_ruang`) VALUES
('L1', 'Ruang Teori 1'),
('L2', 'Ruang Teori 2'),
('L3', 'Ruang Teori 3'),
('L4', 'Ruang Praktek 1'),
('L5', 'Ruang Praktek 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `nim` varchar(10) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` text,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`nim`, `nama`, `alamat`, `email`) VALUES
('3332221100', 'Nursiah', 'Riau', 'nursiah@gmail.com'),
('3332221111', 'Yuliana Bakar', 'Cipondoh', 'yuliana@gmail.com'),
('3332221112', 'Fitriani Bakar', 'Cikokol', 'fitriani@gmail.com'),
('3332221113', 'Desy Bakar', 'Bonang', 'desybakar@gmail.com'),
('3332221114', 'Muhammad Anwar Anugrah', 'Riau', 'anwarbakar@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `username`
--

CREATE TABLE `username` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `aktif` varchar(1) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `username`
--

INSERT INTO `username` (`username`, `password`, `nama`, `aktif`, `email`) VALUES
('admin', 'sukses889', 'Administrator', 'Y', 'admin@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kd_kelas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
